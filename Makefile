IDIR = ./FwLib_STC8/include
LIBSRC = ./FwLib_STC8/src
BUILD_FLAGS = -D__CONF_FOSC=36864000UL -D__CONF_MCU_MODEL=MCU_MODEL_STC8H8K64U -D__CONF_CLKDIV=0x02 -D__CONF_IRCBAND=0x03 -D__CONF_VRTRIM=0x19 -D__CONF_IRTRIM=0x28 -D__CONF_LIRTRIM=0x00
SRC = main.c
BUILDDIR = ./build
MAINDIR = $(BUILDDIR)/main
RELDIR = $(BUILDDIR)/rel
RELSRC = $(wildcard $(LIBSRC)/*.c)
RELFILE = $(patsubst $(LIBSRC)%.c,$(RELDIR)%.rel,$(RELSRC))

all: build $(RELFILE)
	sdcc-sdcc $(SRC) $(RELFILE) $(BUILD_FLAGS) -I $(IDIR) -o $(MAINDIR)/flash.hex

build:
	mkdir -p build/rel build/main

$(RELDIR)/%.rel: $(LIBSRC)/%.c 
	sdcc-sdcc -c $^ $(RELFILE) $(BUILD_FLAGS) -I $(IDIR) -o $(RELDIR)/

clean:
	rm -rf build